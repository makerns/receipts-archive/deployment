# Deployment instructions

These are instructions for deploying Receipts Archive using docker-compose. This is a much easier
way to deploy multiple services when using Docker and in this case, you'll need just one config
file.

## Install Docker

First you need to have Docker and docker-compose installed. You can find Docker installation
instructions [here](https://docs.docker.com/engine/install/) and docker-compose installation
instructions [here](https://docs.docker.com/compose/install/).

If you're using Fedora, you'll need to use Podman and podman-compose instead of Docker. To install
Podman and podman-compose run
```sh
$ sudo dnf install podman podman-compose
```

In the rest of the instructions use `podman` and `podman-compose` commands instead of `docker` and
`docker-compose` respectively. All command options are identical.

## Deploy

Clone this repo to get all required files for deployment.

To deploy the Receipts Archive you'll need to have `docker-compose.yaml` file. That is a config file
that docker-compose will use to setup and deploy the application.

In the docker-compose config file, add the required environment variables like
`SESSION_COOKIE_SECRET` and `GOTHIC_COOKIE_SECRET`. Set the to a "secret" string. You must not share
it with anyone since it's used to encrypt the cookies. A best way to generate secrets is to use a
random string or a number.

An easy way to generate one on Linux is to run
```sh
$ head -c32 /dev/urandom | hexdump -v -e '1/1 "%02x"'
```
This will output a hex string that you can use as a secret. You should use two different secrets.

The `GOOGLE_OAUTH` environment variables are used for authenticating via Google.

TODO: Instructions for creating Google API credentials.

You should change `http://example.com` to the domain or URL you want to use. If you're testing this
out on your local machine, you can use `http://localhost:port`. Just keep in mind that the URL in
environment variables need to match the one in client id settings on Google Developers Console.

To access frontend and backend service, a reverse proxy is used. Configuration for the reverse proxy
is in `nginx` directory.

To deploy everything, run the following command
```sh
$ docker-compose up
```

## Contact

### Maintainers
[Dušan Simić](https://github.com/dusansimic)
